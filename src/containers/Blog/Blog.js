import React, { Component } from 'react';
//import axios from 'axios';
import axios from '../../axios';

import Post from '../../components/Post/Post';
import FullPost from '../../components/FullPost/FullPost';
import NewPost from '../../components/NewPost/NewPost';
import './Blog.css';

class Blog extends Component {
    state = {
        posts: [],
        selectedPost:null
    }
    componentDidMount(){
        axios.get('/posts')
            .then(response => {
                console.log(response);
                const posts = response.data.slice(0,4);
                const updatedPosts = posts.map(post => {
                    return {
                        ...post,
                        author: 'Bhagwan'
                    }
                });
                this.setState({posts:updatedPosts})
            })
            .catch(error => {
                console.log(error);
            });
    }
    render () {
        const posts = this.state.posts.map(post => {
            return <Post key = {post.id} author = {post.author} title = {post.title}
                clicked = {() => this.postSelectedHandler(post.id)}
                />
        });
        return (
            <div>
                <section className="Posts">
                    {posts}
                </section>
                <section>
                    <FullPost id = {this.state.selectedPost}/>
                </section>
                <section>
                    <NewPost />
                </section>
            </div>
        );
    }

    postSelectedHandler = (id) => {
        this.setState({selectedPost:id});
    }
}

export default Blog;